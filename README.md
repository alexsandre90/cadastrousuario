# CadastroUsuario

Projeto para cadastro de usuário utilizando Java, SpringBoot, SpringSecurity, SpringTest, JUnit, MongoDB, Bootstrap e Thymeleaf.

-----------------------------------
As configurações do banco estão na classe MongoConfig. O nome do banco é "infobasedb" e da collection é "usuario";
Não possui usuário nem senha.

-----------------------------------
Foi utilizado SpringSecurity para autenticar o usuário e a senha foi convertida para BCryptEncoder.
Os campos Nome, E-mai, CPF e Password são dados obrigatórios. O formato do CPF é válidado e o desenvolvedor deve pesquisar um site
para gerar CPF's caso deseja adicionar usuários para teste.
A URL "/url-magica-maluca-oajksfbvad6584i57j54f9684nvi658efnoewfmnvowefnoeijn" 
ao ser executada cria um usuário ADMIN com e-mail "admin@gmail.com" e senha "123456".
Somente o usuário ADMIN pode criar, editar todos os perfis, conceder permissão e deletar outros usuários.
Os usuários comuns podem editar o próprio perfil e consultar a lista de perfis cadastrados.