package br.com.infobase.cadastro.controller;

import static org.hamcrest.CoreMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.validation.BindingResult;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.infobase.cadastro.model.Role;
import br.com.infobase.cadastro.model.Usuario;
import br.com.infobase.cadastro.model.UsuarioBuilder;
import br.com.infobase.cadastro.service.UsuarioService;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class CadastroControllerTest {
	
	private UsuarioBuilder builder = new UsuarioBuilder();
	private List<Usuario> users = new ArrayList<Usuario>();
	
	@Mock
	private UsuarioService service;
	
	@Mock
	private BindingResult bind;

	@Autowired
	private MockMvc mock;
	
	@Before
	public void criarUsuarioParaService() {
		users = builder.comNome("Teste builder").comCpf("665.527.180-04").comEmail("teste@gmail.com")
				.comPassword("123456").comTelefone("21-5555-5555").comRole(Role.USER).quantidade(5).buildAll();
		
	}
	
	
	@Test
	public void testUrlPrincipalEAtributos() throws Exception {
		when(service.obterTodos()).thenReturn(users);
		
		mock.perform(MockMvcRequestBuilders.get("/"))
		.andExpect(MockMvcResultMatchers.model().attributeExists("usuarios"))
		.andExpect(MockMvcResultMatchers.model().attribute("admin", false))
		.andExpect(status().isOk());
	}
	
	@Test
	public void testUrlFormulario() throws Exception {
		mock.perform(MockMvcRequestBuilders.get("/form"))
		.andExpect(MockMvcResultMatchers.model().attribute("roles", Role.values()))
		.andExpect(MockMvcResultMatchers.model().attribute("usuario", any(Usuario.class)))
		.andExpect(MockMvcResultMatchers.model().attribute("admin", false))
		.andExpect(status().isOk());
	}
	
	@Test
	public void testUrlCriar() throws Exception{
		mock.perform(MockMvcRequestBuilders.get("/criar"))
		.andExpect(MockMvcResultMatchers.model().attribute("usuario", any(Usuario.class)))
		.andExpect(MockMvcResultMatchers.model().attribute("roles", Role.values()))
		.andExpect(status().isOk());
	}
	
	@Test
	public void testUrlDeletar() throws Exception{
		mock.perform(MockMvcRequestBuilders.delete("/deletar?id="))
		.andExpect(MockMvcResultMatchers.flash().attributeExists("sucesso"))
		.andExpect(MockMvcResultMatchers.redirectedUrl("/"))
		.andExpect(status().is3xxRedirection());
	}
	
	@Test(expected=AssertionError.class)
	public void testErrorAdicionaUsuario() throws Exception{
		Usuario usuario = new Usuario();
		usuario.setRole(Role.USER);
		usuario.setTelefone("21-5555-5555");
				
	    when(service.validarUsuario(usuario)).thenReturn(false);
	    doNothing().when(service).salvarUsuario(usuario);
	    when(bind.hasErrors()).thenReturn(false);
	    mock.perform(
	            post("/confirmar")
	                    .contentType(MediaType.APPLICATION_JSON)
	                    .content(asJsonString(usuario)))
	            .andExpect(status().isCreated())
	            .andExpect(MockMvcResultMatchers.redirectedUrl("/"))
	            .andExpect(status().is4xxClientError());
	}
	
	
	public static String asJsonString(final Object obj) {
	    try {
	        final ObjectMapper mapper = new ObjectMapper();
	        final String jsonContent = mapper.writeValueAsString(obj);
	        return jsonContent;
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}  

}
