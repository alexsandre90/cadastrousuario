package br.com.infobase.cadastro.model;

import java.util.ArrayList;
import java.util.List;

public class UsuarioBuilder {

	
	private Usuario user = new Usuario();
	private List<Usuario> users = new ArrayList<>();
	
	public UsuarioBuilder comNome(String nome) {
		user.setNome(nome);
		return this;
	}
	
	public UsuarioBuilder comCpf(String cpf) {
		user.setCpf(cpf);
		return this;
	}
	
	public UsuarioBuilder comEmail(String email) {
		user.setEmail(email);
		return this;
	}
	
	public UsuarioBuilder comPassword(String password) {
		user.setPassword(password);
		return this;
	}
	
	public UsuarioBuilder comTelefone(String telefone) {
		user.setTelefone(telefone);
		return this;
	}
	
	public UsuarioBuilder comRole(Role role) {
		user.setRole(role);
		return this;
	}
	
	public Usuario build(){
		return user;
	}
	
	public UsuarioBuilder quantidade(int num) {
		for(int i = 0; i < num; i++) {
			users.add(i, user);
			user.setNome(user.getNome() + i);
		}
		
		return this;
	}
	
	public List<Usuario> buildAll(){
		return users;
	}
	
	
}
