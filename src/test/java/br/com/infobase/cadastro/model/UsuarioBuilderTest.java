package br.com.infobase.cadastro.model;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

public class UsuarioBuilderTest {

	@Test
	public void test() {
		UsuarioBuilder builder = new UsuarioBuilder();
		
		List<Usuario> users = builder.comNome("Teste builder").comCpf("665.527.180-04").comEmail("teste@gmail.com")
				.comPassword("123456").comTelefone("21-5555-5555").comRole(Role.USER).quantidade(5).buildAll();
		
		assertEquals(5, users.size());
	}

}
