package br.com.infobase.cadastro.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.infobase.cadastro.CadastroApplication;
import br.com.infobase.cadastro.dao.UsuarioDao;
import br.com.infobase.cadastro.model.Role;
import br.com.infobase.cadastro.model.Usuario;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes= {CadastroApplication.class, UsuarioService.class, UsuarioDao.class})
public class UsuarioServiceTest {

	@Autowired
	private UsuarioService service;
	
	
	@Test
	public void testEncontraPorUserName() {
		Usuario user = new Usuario();
		user.setNome("Teste Usuário");
		user.setCpf("033.642.050-19");
		user.setEmail("teste@gmail.com");
		user.setPassword("123456");
		user.setRole(Role.USER);
		user.setTelefone("21-97995-1006");
		
		service.salvarUsuario(user);
		
		Usuario teste = service.encontre("teste@gmail.com");
		
		assertEquals(teste.getNome(), user.getNome());
	}
	
	@Test
	public void testAdicionaUmUsuario() {
		Usuario user = new Usuario();
		user.setNome("User Teste");
		user.setCpf("144.058.297-13");
		user.setEmail("teste@gmail.com");
		user.setTelefone("(21) 5555-5555");
		user.setPassword("123456");
		user.setRole(Role.USER);
		
		service.salvarUsuario(user);
	}
	
	
	@Test
	public void testRemovePorId() {
		
		Usuario user = new Usuario();
		user.setNome("User Teste");
		user.setCpf("144.058.297-13");
		user.setEmail("teste@gmaill.com");
		user.setTelefone("(21) 5555-5555");
		user.setPassword("123456");
		user.setRole(Role.USER);
		
		service.salvarUsuario(user);
		
		String id = user.getId();
		
		service.removePorId(id);
	}
	
	
	@Test
	public void testEncontrePorId() {
		Usuario usuario = new Usuario();
		usuario.setNome("Admin");
		usuario.setCpf("144.058.297-13");
		usuario.setEmail("teste@gmaill.com");
		usuario.setTelefone("(21) 5555-5555");
		usuario.setPassword("123456");
		usuario.setRole(Role.USER);
		
		service.salvarUsuario(usuario);
		
		Usuario user = service.encontrePorId(usuario.getId());
		
		assertEquals("Admin", user.getNome());
	}
	
	@Test
	public void testLoginSucess() {
		Usuario usuario = new Usuario();
		usuario.setNome("Admin");
		usuario.setCpf("144.058.297-13");
		usuario.setEmail("teste@gmaill.com");
		usuario.setTelefone("(21) 5555-5555");
		usuario.setPassword("123456");
		usuario.setRole(Role.USER);
		
		service.salvarUsuario(usuario);
		
		assertTrue(service.login("teste@gmaill.com", "123456"));
	}
	
	
	@Test(expected=IllegalArgumentException.class)
	public void testvalidarUsuario() {
		Usuario usuario = new Usuario();
		usuario.setNome("Admin");
		usuario.setCpf("144.058.297-13");
		usuario.setEmail("teste@gmaill.com");
		usuario.setTelefone("(21) 5555-5555");
		usuario.setPassword("123456");
		usuario.setRole(Role.USER);
		
		service.salvarUsuario(usuario);
		
		service.validarUsuario(usuario);
		
		
	}
		
	@After
	public void rollback() {
		service.removeTodos();
	}

}
