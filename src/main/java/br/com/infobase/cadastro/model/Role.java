package br.com.infobase.cadastro.model;

import org.springframework.data.annotation.Id;
import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority{

	
	USER("USER"),
	ADMIN("ADMIN");
	
	@Id
	public final String _id;
	
	Role(String name) {
		this._id = name;
	}
	
	@Override
	public String getAuthority() {
		return this._id;
	}
}
