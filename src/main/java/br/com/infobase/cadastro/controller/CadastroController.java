package br.com.infobase.cadastro.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.infobase.cadastro.model.Role;
import br.com.infobase.cadastro.model.Usuario;
import br.com.infobase.cadastro.service.UsuarioService;

@Controller
public class CadastroController {

	@Autowired
	private UsuarioService service;

	@RequestMapping("/")
	public ModelAndView home(HttpSession session) {

		ModelAndView model = new ModelAndView("/home");
		Usuario userSession = (Usuario) session.getAttribute("usuarioLogado");
		
		boolean admin = checkAdmin(userSession);
		
		model.addObject("admin", admin);
		model.addObject("usuarios", service.obterTodos());

		return model;
	}

	
	// formulario padrão
	@RequestMapping("/form")
	public ModelAndView form(Usuario user, Role role, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("/form");

		Usuario userSession = (Usuario) request.getSession().getAttribute("usuarioLogado");

		boolean admin = checkAdmin(userSession);
		
		model.addObject("roles", Role.values());
		model.addObject("usuario", user);
		model.addObject("admin", admin);

		return model;
	}

	// metodo para salvar o usuario que foi editado
	@RequestMapping(value = "/salvar", method = RequestMethod.PUT)
	public ModelAndView salvar(@Valid Usuario user, Role role ,BindingResult bind, RedirectAttributes redirect, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("redirect:/");
		
		if (bind.hasErrors()) {
			return new ModelAndView("redirect:/form");
		}
		
		if(role == null) {
			role = Role.USER;
		}

		redirect.addFlashAttribute("sucesso", "Usuário " + user.getEmail() + " editado com sucesso!");
		user.setRole(role);
		service.salvarUsuario(user);

		return model;
	}

	// metodo para editar as informações do usuario informado pelo ID
	@RequestMapping("/editar")
	public ModelAndView editar(@RequestParam("id") String id, HttpServletRequest request) {
		ModelAndView model = new ModelAndView("/form");

		Usuario user = service.encontrePorId(id);
		Usuario userSession = (Usuario) request.getSession().getAttribute("usuarioLogado");
		boolean admin = checkAdmin(userSession);

		model.addObject("admin", admin);
		model.addObject("usuario", user);
		model.addObject("roles", Role.values());
		return model;
	}

	

	// metodo responsavel por deletar o usuario com o ID passado por parametro
	@PreAuthorize(value = "ADMIN")
	@RequestMapping(value="/deletar")
	public ModelAndView deletar(@RequestParam("id") String id, HttpSession session, RedirectAttributes redirect) {
		ModelAndView model = new ModelAndView("redirect:/");
		
		service.removePorId(id);
		
		Usuario userLogado = (Usuario) session.getAttribute("usuarioLogado");
		
		// caso o usuario logado delete a sua propria conta, a session eh invalidada
		if(userLogado != null) {
			if(userLogado.getId().equals(id)) {
				session.invalidate();
			}
		}
		
		redirect.addFlashAttribute("sucesso", "Usuário deletado com sucesso!");
		return model;
	}
	
	// metodo para criar uma nova conta
	@RequestMapping("/criar")
	public ModelAndView criar(Usuario user, Role role) {
		ModelAndView model = new ModelAndView("/criar");
		
		model.addObject("usuario", user);
		model.addObject("roles", Role.values());		
		return model;
	}
	
	// metodo para confirmar a criação de uma conta
	@RequestMapping(value="/confirmar", method=RequestMethod.POST)
	public ModelAndView confirmar(@Valid Usuario user, Role role ,BindingResult bind, RedirectAttributes redirect) {
		ModelAndView model = new ModelAndView("redirect:/");
		
		// caso haja erro no formulario ou o metodo validarUsuario retornar falso (ja existe email e cpf cadastrados) retornara para o metodo de criar usuario
		if(bind.hasErrors() || !service.validarUsuario(user)) {
			return criar(user, role);
		}
		
		redirect.addFlashAttribute("sucesso", "Usuário " + user.getEmail() + " criado com sucesso!");
		
		// eh definido a ROLE de USER por padrão
		user.setRole(Role.USER);
		service.salvarUsuario(user);
		
		return model;
	}

	
	// URL para criar um usuario ADMIN, a URL não pode ser executada duas vezes pois ela valida se o usuario ADMIN já existe
	@RequestMapping("/url-magica-maluca-oajksfbvad6584i57j54f9684nvi658efnoewfmnvowefnoeijn")
	public ModelAndView urlMagica(RedirectAttributes redirect) {
		ModelAndView model = new ModelAndView("redirect:/");


		Usuario user = new Usuario();
		user.setNome("Admin");
		user.setRole(Role.ADMIN);
		user.setEmail("admin@gmail.com");
		user.setTelefone("55-5555-5555");
		user.setPassword("123456");
		user.setCpf("144.058.297-13");

		if(service.validarUsuario(user)) {
			service.salvarUsuario(user);
			redirect.addFlashAttribute("sucesso", "Usuario " + user.getEmail() + " adicionado com sucesso!");
		}

		return model;
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView loginForm() {
		return new ModelAndView("/login");
	}
	
	// logout e invalida a sessão
	@RequestMapping(value = "/logout")
	public ModelAndView logout(HttpSession session) {
		
		session.invalidate();
		
		return new ModelAndView("redirect:/");
	}

	
	// metodo criado para validar o login do usuario
	@RequestMapping(value = "/check", method = RequestMethod.POST)
	public String check(String username, String password, HttpSession session, RedirectAttributes redirect) {
		Usuario user = null;

		if (!service.login(username, password)) {
			redirect.addFlashAttribute("erro", "Os dados informados estão errados!");
			return "login.html";
		}

		user = service.encontre(username);
		session.setAttribute("usuarioLogado", user);

		return "redirect:/";
	}
	
	
	// metodo privado para validar se o usuario da session tem permissão de admin
	private boolean checkAdmin(Usuario userSession) {
		if (userSession != null) {
				if (userSession.getRole().equals(Role.ADMIN)) {
					return true;
				}
			}
		return false;
	}

}
