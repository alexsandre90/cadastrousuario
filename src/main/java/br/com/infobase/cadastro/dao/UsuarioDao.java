package br.com.infobase.cadastro.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.infobase.cadastro.model.Usuario;
import br.com.infobase.cadastro.repository.UsuarioRepository;

@Repository
@Transactional
@Component
public class UsuarioDao implements UserDetailsService{

	@Autowired
	private UsuarioRepository repository;
	
	@Autowired
	private MongoTemplate mongo;
	
	public void adiciona(Usuario usuario) {
		repository.save(usuario);
	}

	public void removeTodos() {
		repository.deleteAll();
	}

	@Override
	public Usuario loadUserByUsername(String email) throws UsernameNotFoundException {
		Query query = new Query();
		query.addCriteria((Criteria.where("email").regex(email, "i")));
		
		Usuario user = mongo.findOne(query, Usuario.class);
		
		return user;
	}

	public List<Usuario> findAll() {
		
		return repository.findAll();
	}

	public void removePorId(String id) {
		repository.deleteById(id);
	}

	public Usuario encontrePorId(String id) {
		Usuario user = mongo.findById(id, Usuario.class);
		return user;
	}

	public Usuario encontrePorCPF(String cpf){
		Query query = new Query();
		query.addCriteria(Criteria.where("cpf").regex(cpf, "i"));
		
		Usuario user = mongo.findOne(query, Usuario.class);
		
		return user;
	}


	
}
