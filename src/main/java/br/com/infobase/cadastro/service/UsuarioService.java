package br.com.infobase.cadastro.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.infobase.cadastro.dao.UsuarioDao;
import br.com.infobase.cadastro.model.Usuario;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioDao dao;

	private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

	public void salvarUsuario(Usuario usuario) {
		
		String password = bCryptPasswordEncoder.encode(usuario.getPassword());
		usuario.setPassword(password);

		dao.adiciona(usuario);

	}

	public boolean validarUsuario(Usuario usuario) {

		Usuario userEmail = dao.loadUserByUsername(usuario.getEmail());
		Usuario userCpf = dao.encontrePorCPF(usuario.getCpf());

		if (userEmail == null & userCpf == null) {
			return true;
		} else {

			throw new IllegalArgumentException("Já existe usuário com os dados informados");
		}
	}

	public void removeTodos() {
		dao.removeTodos();
	}

	public List<Usuario> obterTodos() {
		List<Usuario> usuarios = dao.findAll();
		return usuarios;
	}

	public boolean login(String username, String password) {
		Usuario user = dao.loadUserByUsername(username);
	
		if(user == null) {
			return false;
		}

		boolean validaPassword = bCryptPasswordEncoder.matches(password, user.getPassword());

		return validaPassword;
	}

	public Usuario encontre(String username) {
		return dao.loadUserByUsername(username);
	}

	public void removePorId(String id) {
		dao.removePorId(id);
	}

	public Usuario encontrePorId(String id) {
		Usuario user = dao.encontrePorId(id);

		return user;
	}
}
